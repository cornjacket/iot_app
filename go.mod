module bitbucket.org/cornjacket/cmd_hndlr_appnum4_appapi3_sysapi6

go 1.13

require (
	bitbucket.org/cornjacket/iot v0.1.0
	github.com/julienschmidt/httprouter v1.3.0
)
