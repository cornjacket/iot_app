package controllers

import (
	// Standard library packets
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
)

type (
	PktForwarderController struct{
		eventURL	string
		atcURL		string
		appNum		int
		appApi		int
		sysApi		int
		enabled		bool
	}
)

var serviceName string = "CMD_HNDLR"

func NewPktForwarderController(appNum int, appApi int, sysApi int, eventURL string, atcURL string) *PktForwarderController {
	controller := &PktForwarderController{}
	if eventURL != "" && atcURL != "" {
		controller.eventURL = eventURL
		controller.atcURL = atcURL
		controller.appNum = appNum
		controller.appApi = appApi
		controller.sysApi = sysApi
		controller.enabled = true
	}
	return controller
}

func (pfc PktForwarderController) PktForwarderPacketHandler(wID int, packet message.UpPacket) bool {
	//fmt.Printf("%s\tCONTROLLER\tDEBUG\tPktForwarderPacketHandler: invoked", serviceName)


	//fmt.Printf("%s\tCONTROLLER\tDEBUG\tPktForwarderPacketHandler: destURL: %s", serviceName, pfc.destURL)

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(packet)
	emptyEventUrl := pfc.eventURL == ""
	emptyAtcUrl := pfc.atcURL == ""
	isCmdRx := packet.Cmd == "rx"
	isCmdGw := packet.Cmd == "gw"
	// here we are checking if there is an invalid string, maybe include this with the enabled check above 
	if !emptyEventUrl && isCmdRx {
		fmt.Printf("%s\tPktHandler: worker %d sending packet to %s", serviceName, wID, pfc.eventURL)
		if pfc.enabled {
			res, _ := http.Post(pfc.eventURL, "application/json; charset=utf-8", b)
			io.Copy(os.Stdout, res.Body)
		}
		fmt.Println()
	} else if !emptyAtcUrl && isCmdGw {
		fmt.Printf("%s\tPktHandler: worker %d sending packet to %s", serviceName, wID, pfc.atcURL)
		if pfc.enabled {
			res, _ := http.Post(pfc.atcURL, "application/json; charset=utf-8", b)
			io.Copy(os.Stdout, res.Body)
		}
		fmt.Println()
	} else {
		fmt.Printf("%s\tPktHandler: worker %d: Not forwarding packet. cmd==rx: %t, cmd=gw: %t\n", serviceName, wID, isCmdRx, isCmdGw)
	}

	if !pfc.enabled {
		fmt.Printf("%s\tPktHandler: worker %d: PktForwarder is not enabled. ERROR. Should forward to exception route\n", serviceName, wID)
	}

	return true
}
