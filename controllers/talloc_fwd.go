package controllers

import (
	// Standard library packets
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	// Third party packages
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type (
	TAllocRespForwarderController struct{
		destURL	string
		// DRT - not sure if following are needed...
		appNum	int
		appApi	int
		sysApi	int
		enabled	bool
	}
)

//var serviceName string = "TALLOCRESP_FWD"

func NewTAllocRespForwarderController(appNum int, appApi int, sysApi int, destURL string) *TAllocRespForwarderController {
	controller := &TAllocRespForwarderController{}
	if destURL != "" {
		controller.destURL = destURL
		controller.appNum = appNum
		controller.appApi = appApi
		controller.sysApi = sysApi
		controller.enabled = true
	}
	return controller
}

func (trc TAllocRespForwarderController) TAllocRespHandler(wID int, tallocresp tallocrespproc.TAllocResp) bool {
	//fmt.Printf("%s\tCONTROLLER\tDEBUG\tPktForwarderPacketHandler: invoked", serviceName)

	if trc.enabled {

		//fmt.Printf("%s\tCONTROLLER\tDEBUG\tPktForwarderPacketHandler: destURL: %s", serviceName, pfc.destURL)

		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(tallocresp)
		emptyDestUrl := trc.destURL == ""
		// here we are checking if there is an invalid string, maybe include this with the enabled check above 
		if !emptyDestUrl {
			fmt.Printf("TAlloc\tCONTROLLER\tINFO\tTAllocRespHandler: worker %d sending TAllocResp to %s", wID, trc.destURL)

			// TODO: Check for error on return which should disable the controller for a time. What happens to packets that are not forwarded? 
			res, _ := http.Post(trc.destURL, "application/json; charset=utf-8", b)
			io.Copy(os.Stdout, res.Body)
		} else {
			fmt.Printf("TAlloc\tCONTROLLER\tINFO\tTAllocRespHandler: worker %d: Not forwarding TAllocResp. destUrlEmpty: %t\n", wID, emptyDestUrl)
		}

	} else {
		fmt.Printf("TAlloc\tCONTROLLER\tERROR\tTAllocRespHandler: worker %d: TAllocRespForwarder is not enabled. Not forward TAllocResp. Should forward to exception route\n", wID)
	}

	return true
}
