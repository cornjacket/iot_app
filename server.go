package main

import (
	// Standard library packets
	"fmt"
	"net/http"
	"os"
	"time"

	// Third party packages
	"bitbucket.org/cornjacket/iot/clientlib/service"
	"bitbucket.org/cornjacket/cmd_hndlr_appnum4_appapi3_sysapi6/controllers"
	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
	"bitbucket.org/cornjacket/iot/serverlib/lastboot"
	"bitbucket.org/cornjacket/iot/serverlib/version"
	"bitbucket.org/cornjacket/iot/serverlib/shutdown"
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

/*
for i in {1..4096}; do curl localhost:8000/work -d name=$USER -d delay=$(expr $i % 11)s; done

for i in {1..4096}; do curl -X POST   http://localhost:3000/packet  -H 'content-type: application/json; charset=UTF-8'   -d '{"dr": "SF10 BW125 4/5", "ts": 1528765549342, "EUI": "008000000400476E", "ack": false, "bat": 255, "cmd": "rx", "snr": 8.5, "toa": 370, "data": "FE32FF99", "fcnt": 246, "freq": 903300000, "port": 1, "rssi": -41, "seqno": 7755781}'
; done

curl -X POST   http://localhost:3000/packet  -H 'content-type: application/json; charset=UTF-8'   -d '{"dr": "SF10 BW125 4/5", "ts": 1528765549342, "EUI": "008000000400476E", "ack": false, "bat": 255, "cmd": "rx", "snr": 8.5, "toa": 370, "data": "FE32FF99", "fcnt": 246, "freq": 903300000, "port": 1, "rssi": -41, "seqno": 7755781}'
*/

var serviceName string = "CMD_HNDLR_PKT_FWDR" // TODO: This should be generated via function that takes in appNum, appApi, and sysApi

var appNum int = 4
var appApi int = 3
var sysApi int = 6

// TODO: The following are used in the service.Register call below. Review what their purpose is and how to remove.
// for now gloablly store the destId, destUrl, and destStatus for oprouter so we can advertise with discovery
// this should really go into models...
var destId []message.ServiceId
var destUrl []string
var destStatus []message.DestServiceStatus

func main() {

    versionMajor := 0
    versionMinor := 0
    versionPatch := 0

    fmt.Println("DISCOVERY_URL: ", os.Getenv("DISCOVERY_URL"))	// fully qualified url
    accessDiscovery := false
    discoveryEndpoint := os.Getenv("DISCOVERY_URL")
    // should test if discovery endpoint is valid, maybe not here, but somewhere
    if discoveryEndpoint != "" {
	accessDiscovery = true
	fmt.Printf("%s\tMAIN\tINFO\tDiscovery Endpoint = %s\n", serviceName, discoveryEndpoint)
    } else {
	fmt.Printf("%s\tMAIN\tINFO\tDiscovery Endpoint not provided via environment.\n", serviceName)
    }

    fmt.Println("PKT_ROUTER_PORTNUM: ", os.Getenv("PKT_ROUTER_PORTNUM"))
    portNum := os.Getenv("PKT_ROUTER_PORTNUM")
    if portNum == "" {
	portNum = "8080"
	fmt.Printf("%s\tMAIN\tINFO\tPkt_Router Port Num will take default: %s\n", serviceName, portNum)
    } else {
	    fmt.Printf("%s\tMAIN\tINFO\tPkt_Router Port Num provided via environment: %s\n", portNum)
    }

    lastBoot := message.LastBoot(time.Now().Unix())
    serviceID := message.PktForwarderId
    serviceVersion := message.Version{Major: versionMajor, Minor: versionMinor, Patch: versionPatch}
    serviceShutdown := message.Shutdown{Service: serviceID, MagicNumber: 1234}


    // Instantiate a new router
    r := httprouter.New()

    version.NewController(r, serviceVersion)
    shutdown.NewController(r, serviceShutdown, serviceName)
    lastboot.NewController(serviceName, r, lastBoot)
    service.Init(serviceName)

	// This portion sets up the pktproc workers, however at init time there are no init routes
	// pktproc should have a function that adds a route to the pktproc route table, wait are we adding routes or are we adding services first?

	numWorkers := 100
	// TODO: This should be a forward_init struct that gets passed into the newController function below
	// TODO: Later the appNum, appApi, and sysApi will be used for the messageQ topic.
	// TODO: This service should ask discovery for the address of the message q.
	pktEventHndlrDestURL := "http://localhost:8081/packet"
	airTrafficControlDestURL := "http://localhost:8083/packet"
	pfc := controllers.NewPktForwarderController(appNum, appApi, sysApi, pktEventHndlrDestURL, airTrafficControlDestURL)
	tAllocRespEventHndlrDestURL := "http://localhost:8081/tAllocResp.4.3.6"
	tfc := controllers.NewTAllocRespForwarderController(appNum, appApi, sysApi, tAllocRespEventHndlrDestURL)

	// TODO: There needs to be a function defined in this service that takes appNum, appApi, and SysApi and converts into
	// a string or path that looks like the following: "AppNum4AppApi3SysApi6"
	pktproc2.NewPacketController(serviceName, r, numWorkers, pfc.PktForwarderPacketHandler, "AppNum4AppApi3SysApi6") // TODO: change to packet.4.3.6
	tallocrespproc.NewTAllocRespController(serviceName, r, numWorkers, tfc.TAllocRespHandler, "tAllocResp.4.3.6") // what should the url be?

	// TODO: These are outdated. I need to transform into mechanism to change the destination URL for the app data path...
	//r.POST("/oproute", orc.CreateOpRoute) // useful when there is no discovery service
	//r.GET("/oproute/:id", orc.GetOpRoute)


	// TODO: The following needs to be modified to be used with a yet to be defined new Discovery service and client lib
	if accessDiscovery == true {
		// Registering with Discovery should happen after ListenAndService but that blocks,
		// so i need to fork. But not right now.
		fmt.Printf("%s\tMAIN\tINIT\tRegistering with Discovery\n",serviceName)
		opcodeSupport := false // TODO: Remove this concept
		onboardSupport := false
		notifySupport := false
		packetSupport := true
		cardinalitySupport := false
		appNum := message.NullId
		noDataSupport := ""

		// this should check for return value
		service.Register(discoveryEndpoint, portNum, serviceID, appNum, opcodeSupport, onboardSupport, notifySupport, packetSupport, cardinalitySupport, lastBoot, noDataSupport, destId, destUrl, destStatus)
	}

	// Fire up the server
	fmt.Printf("%s\tMAIN\tINIT\tStarting server on port: %s\n",serviceName, portNum)
        if err := http.ListenAndServe(":"+portNum, r); err != nil {
		fmt.Println(err.Error())
		// notify discovery of the failure
	}

}
